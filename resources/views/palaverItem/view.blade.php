<!-- TODO Möglichkeit auf /edit zuzugreifen einfügen -->
@extends('layouts.layout')
@section('content')
    <h1 style="text-align: center">{{$palaverItem->project->name}} - {{$palaverItem->title}}</h1>
    @can('update',$palaverItem)
        <div>
            <a href='/palaverItem/{{$palaverItem->id}}/edit'>
                <button class="btn btn-default">
                    Bearbeiten <span class="glyphicon glyphicon-pencil" style="vertical-align:middle"></span>
                </button>
            </a>
        </div>
    @endcan
    <div class="row">
        <div class="panel panel-default col-md-8 col-md-offset-2">
            <div class="panel-body">
                <dl class="dl-horizontal ">
                    <h4>
                        <dt>Verantwortlich</dt>
                        <dd>{{$palaverItem->responsible_users}}</dd>

                        <dt>Arbeit Woche</dt>
                        <dd>{{Help::format_time($palaverItem->work_this_week)}}</dd>
                        <dt>Arbeit Monat</dt>
                        <dd>{{Help::format_time($palaverItem->work_this_month)}}</dd>
                        <dt>Arbeit Jahr</dt>
                        <dd>{{Help::format_time($palaverItem->work_this_year)}}</dd>

                        @if(!is_null($palaverItem->date))
                            <dt>Termin</dt>
                            <dd>{{\Carbon\Carbon::parse($palaverItem->date)->format('d.m.Y')}}</dd>
                        @else
                            <dt>Termin</dt>
                            <dd>Kein Termin gespeichert</dd>
                        @endif
                        <dt>Status</dt>
                        <dd>{{$palaverItem->status}}</dd>
                    </h4>
                </dl>
            </div>
        </div>
    </div>
    <div class="row">
        <h3>Was bisher geschah:</h3>
        <table class="table table-hover  table-bordered">
            <thead>
            <tr>
                <td>Datum</td>
                <td>Name</td>
                <td>Beschreibung</td>
                <td>Stunden</td>

            </tr>
            </thead>
            @foreach($entries as $entry)
                <tr>
                    <td>{{Help::formatDate($entry->date)}}</td>
                    <td>{{$entry->user->short_name}} </td>
                    <td>{{$entry->description}}</td>
                    <td>{{$entry->formatted_time}}</td>

                    @endforeach
                </tr>
        </table>
        {{$entries->setPath('')->links()}}
    </div>
@endsection
