@extends('layouts.layout')
@section('title','PALAVER')
@section('head')
    <style>
        .glyphicon.large {
            font-size: 35px;
        }
    </style>

@endsection

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="col-md-12">
        <?php $counter = 0?>
        <?php $max = 0;
        foreach ($projects as $project) {
            $max += $project->palaverItems->count(); //amount of palaverItems dispplayed
        }
        $max = $max + $projects->count();

        ?>
        @foreach($projects as $project)


            <div class="panel panel-default col-md-12 project-title col-md-offset-0 "
                 style="z-index: 100; position: relative">
                <h2 class="col-md-10">{{$project->name}}</h2> <a href="/project/{{$project->id}}/edit?type=p"
                                                                 class="pull-right">Edit</a>
                <!-- TODO flugstatus option -->
            </div>

            @foreach($project->palaverItems as $palaverItem)
                <!-- Navigation -->
                    <div class="row col-md-1  {{$palaverItem->id}}_idnav" style="visibility:hidden"
                         id="{{$counter}}_nav">
                    @if($counter !=0)
                        <a onclick="movePointerUp()"> <span class="glyphicon large glyphicon glyphicon-triangle-top"
                                                            aria-hidden="true"></span></a>
                    @else
                        <span class="glyphicon large glyphicon glyphicon-triangle-top" style="visibility: hidden"
                              aria-hidden="true"></span>
                        @endif
                    <!-- action for center button -->
                        <div class="btn-group">
                            <a href="{{$palaverItem->id}}" class=" dropdown-toggle" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false"><span class="glyphicon large glyphicon-pencil"
                                                                                aria-hidden="true"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href=/palaverItem/{{$palaverItem->id}}/edit?type=p>Formular öffnen</a></li>
                                <!-- TODO redirect to palaver/no palaver after done -->
                                <li><a id="{{$counter}}_markDone" onclick=markDone({{$palaverItem->id}})>Als Fertig
                                        markieren</a></li>
                                <li><a id="{{$counter}}_dropdownToggle"
                                       onclick="edit( {{$palaverItem->id}})">Bearbeiten</a></li>
                                <li><a onclick="save()">Speichern</a></li>

                            </ul>
                        </div>
                        @unless($counter>$max-2)
                            <a onclick="movePointerDown()"><span
                                        class="glyphicon large glyphicon glyphicon-triangle-bottom"
                                        aria-hidden="true"></span></a>
                        @endunless
                </div>
                <div class="col-md-11">
                    <div class="panel panel-default {{$palaverItem->id}}_idnav" id="div_{{$counter}}">
                        <div class="panel-heading"><h4>{{$palaverItem->title}}</h4></div>
                        <div class="panel-body" id="{{$palaverItem->id}}">


                            <div class="row col-md-6">
                                <dl class="dl-horizontal">

                                    <h4>
                                        <dt>Verantwortlich</dt>
                                        <dd id="{{$palaverItem->id}}_users">{{$palaverItem->responsible_users}}</dd>

                                        <dt>Beschreibung</dt>
                                        <dd id="{{$palaverItem->id}}_description">{{$palaverItem->description}}</dd>

                                        <dt>Gesamtbauzeit</dt>
                                        <dd>{{$palaverItem->formatted_work_time}}</dd>
                                        <dt>Aktueller Status</dt>
                                        <dd id={{$palaverItem->id}}_status>{{$palaverItem->real_status}}</dd>
                                        <dt>Termin</dt>
                                        <dd id="{{$palaverItem->id}}_date">{{$palaverItem->date}}</dd>
                                    </h4>
                                </dl>
                            </div>
                            <div class="row col-md-6">
                                @if(!$palaverItem->entries->isEmpty())
                                    <table class="table table-hover  table-bordered">
                                        @foreach($palaverItem->entries->sortBy('date')->chunk(3)[0] as $entry)
                                            <tr>
                                                <td>{{$entry->user->short_name}}</td>
                                                <td> {{$entry->description}}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <?php $counter++?>
                @endforeach
            <!-- Sonstiges -->
                <div class="row col-md-1" style="visibility:hidden" id="{{$counter}}_nav">
                    @if($counter !=0)
                        <a onclick="movePointerUp()"> <span class="glyphicon large glyphicon glyphicon-triangle-top"
                                                            aria-hidden="true"></span></a>
                    @else
                        <span class="glyphicon large glyphicon glyphicon-triangle-top" style="visibility: hidden"
                              aria-hidden="true"></span>
                    @endif

                    <span id="{{$counter}}_dropdownToggle"
                          onclick="redirect('/palaverItem/create?project={{$project->id}}&type=p')">
                    <span class="glyphicon large glyphicon-pencil" style="color:#337ab7;" aria-hidden="true"></span>
                </span>
                    @unless($counter>$max-2)
                        <a onclick="movePointerDown()"><span
                                    class="glyphicon large glyphicon glyphicon-triangle-bottom"
                                    aria-hidden="true"></span></a>
                    @endunless
                </div>
                <div class="col-md-11">

                    <div class="panel panel-default" id="div_{{$counter}}">
                        <?php $counter++ ?>
                        <div class="panel-heading"><h4>Sonstiges {{$project->name}}</h4></div>
                        <div class="panel-body">

                            <a href="/palaverItem/create?project={{$project->id}}&type=p"
                               class="btn btn-warning col-md-offset-5">
                                <h5>Weiteren Eintrag
                                    erstellen</h5></a>
                    </div>
                </div>
                </div>

        @endforeach

            {!!  Form::open(['method'=>'post','url' => '/palaver_save_data','class' => 'form-horizontal','role'=>'form']) !!}

            <div class="form-group">
                {!!Form::label('comments','Anmerkungen',['class' => 'control-label col-sm-2 pull-left'])!!}
                <div class="col-sm-8 col-sm-offset-1">
                    {!!Form::textarea('Anmerkungen',NULL,['class' => 'form-control','rows'=>'3'])!!}
                </div>
            </div>

    </div>


    <div class="col-md-12 col-md-offset-0">
        <div class="panel panel-default ">
            <div class="panel-heading"><h4>Palaver Beenden</h4></div>
            <div class="panel-body">

                {!! Form::submit('Zusammenfassung anzeigen',['class' => 'btn btn-success col-md-3 col-md-offset-4     col-xs-6 col-sm-2 col-sm-offset-8']) !!}


                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection



@section('afterBody')
    <script>


        var pointer_pos = 0;
        var cur_id = -1; //track if something is currently being updated
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            }
        });
        //pos is current position of the pointer
        function movePointerUp() {
            if (pointer_pos > 0) {
                if (cur_id != -1) {
                    save();
                }

                document.getElementById(pointer_pos + "_nav").style.visibility = "hidden";
                document.getElementById(pointer_pos - 1 + "_nav").style.visibility = "visible";
                var pp = pointer_pos - 1;

                window.scrollTo(0, $(window).scrollTop() + $('#div_' + pp).offset().top - $('#div_' + pointer_pos).offset().top);

                pointer_pos = pointer_pos - 1;
            }
            return false;
        }

        function movePointerDown() {

            if (pointer_pos < {{$max-1}}) {

                if (cur_id != -1) {
                    save();
                }
                document.getElementById(pointer_pos + "_nav").style.visibility = "hidden";
                document.getElementById(pointer_pos + 1 + "_nav").style.visibility = "visible";
                var pp = pointer_pos + 1;

                window.scrollTo(0, $(window).scrollTop() + $('#div_' + pp).offset().top - $('#div_' + pointer_pos).offset().top); //TODO cant read offset of Sonstiges Button

                pointer_pos = pointer_pos + 1;
            }
            return false;
        }

        $(document).ready(function () {
            if ($(window).width() > 992) {
                $(".project-title").stick_in_parent();
                @unless(Session::has('hook'))
                document.getElementById('0_nav').style.visibility = "visible";
                @else

                document.getElementsByClassName({{Session::get('hook')}}+'_idnav')[0].style.visibility = "visible";

                window.scrollTo(0, $('.{{Session::get('hook')}}_idnav').offset().top - 100);
                // document.getElementsByClassName({{Session::get('hook')}}+"_idnav")[0].scrollIntoView();

                var temp = $('.' + '{{Session::get('hook')}}' + '_idnav').attr('id');
                pointer_pos = parseInt(temp.substr(0, temp.indexOf('_')));


                @endif

                $('body').keydown(function (event) {
                    if (event.keyCode == 13) {//Enter
                        event.preventDefault();
                        if (cur_id == -1) {

                            $('#' + pointer_pos + "_dropdownToggle").trigger('click');
                        }
                        else {
                            save();
                        }

                    }
                    else if (event.keyCode == 37) { //left arrow
                        movePointerUp();
                    }
                    else if (event.keyCode == 39) { //right arrow
                        movePointerDown();
                    }
                    else if (event.keyCode == 68 && cur_id == -1) { //d key
                        $('#' + pointer_pos + "_markDone").click();
                    }
                });

            }
            else {
                alert("Minimum display widht 992 px")
            }
        });

        function redirect(url) {
            $(location).attr('href', url);
        }

        function edit(id) {
            if (cur_id != -1) {
                console.log(cur_id);
                alert("Bitte Änderungen zuerst speichern");
                return false;
            }
            cur_id = id;

            //make multiselect editable
            $.post("/palaverItem/" + id + "/ajax", {
                        'action': 'user_form',
                    },
                    function (data) {
                        $('#' + id + '_users').html(data);
                        console.log(data);
                    })

            //make description editable
            cur_val = $('#' + id + '_description').html();
            var newHTML = '<textarea class="form-control" rows="2" name="description" cols="50" id="description_edit">' + cur_val + '</textarea>';
            $('#' + id + '_description').html(newHTML);


            //make status editable
            cur_status = $('#' + id + '_status').html();
            if (cur_status != "Fertig" && cur_status != "Abgebrochen") {
                var newHTML = '<textarea class="form-control" rows="2" name="status" cols="50" id="status_edit">' + cur_status + '</textarea>';
                $('#' + id + '_status').html(newHTML);
            }

            //make date editable
            cur_date=$('#' + id + '_date').html();
            var newHTML = '<textarea class="form-control" rows="2" name="status" cols="50" id="date_edit">' + cur_date + '</textarea>'
            $('#' + id + '_date').html(newHTML);
            return;
        }

        function save() {
            var id = cur_id
            //fetch new text for description
            var new_desc = $('#description_edit').val();
            var new_status = $('#status_edit').val()
            var new_date = $('#date_edit').val()

            //get selected ids

            var ids = [];
            $('.selectpicker option:selected').each(function () {
                ids.push($(this).val());
            });


            //send ajax to save new values
            $.post("/palaverItem/" + id + "/ajax", {
                        'action': 'edit_all',
                        'description': new_desc,
                        'helpers': ids,
                        'status': new_status,
                        'date': new_date
                    },
                    function (data) {
                        $('#' + id + '_users').html(data[0]);
                        $('#' + id + '_description').html(data[1]);
                        $('#' + id + '_status').html(data[2]);
                        $('#' + id + '_date').html(data[3]);

                    })
            cur_id = -1;
        }


        function markDone(id) {


            $.post("/palaverItem/" + id + "/ajax", {
                        'action': 'status',
                    },
                    function (data) {
                        $('#' + id + '_status').html(data);
                    })

        }
    </script>
@endsection