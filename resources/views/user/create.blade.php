@extends('layouts.outer_form_partial')
@section('title')
    Mitglied Hinzufügen
@endsection


@section('panel_heading','Mitglied Hinzufügen')
@section('panel_body')
    {!!  Form::open(['method'=>'post','url' => 'register/','class' => 'form-horizontal','role'=>'form']) !!}
    @include('user.form_partial')


    @include('layouts.form_buttons_partial')

    {!! Form::close() !!}

@endsection
