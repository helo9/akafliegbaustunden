
@if(Auth::check()&&Auth::user()->is_admin)
    <div class="form-group">
        {!!Form::label('first_name','Vorname',['class' => 'control-label col-sm-2'])!!}
        <div class="col-sm-10">
            {!!Form::text('first_name',null,['class' => 'form-control','required' => 'required'])!!}
        </div>
    </div>

    <div class="form-group ">
        {!!Form::label('nickname','Spitzname',['class' => 'control-label col-sm-2'])!!}
        <div class="col-sm-10">
            {!!Form::text('nickname',null,['class' => 'form-control col-sm-10'])!!}
        </div>
    </div>

    <div class="form-group">
        {!!Form::label('last_name','Nachname',['class' => 'control-label col-sm-2'])!!}
        <div class="col-sm-10">
            {!!Form::text('last_name',null,['class' => 'form-control','required' => 'required'])!!}
        </div>
    </div>

    <div class="form-group">
        {!!Form::label('status','Status',['class' => 'control-label col-sm-2'])!!}
        <div class="col-sm-10">
            {!!Form::select('status_id',$statuses,null,['class' => 'form-control col-sm-4','required' => 'required'])!!}
        </div>
    </div>

    <div class="form-group">
        {!!Form::label('email','E-Mail Adresse',['class' => 'control-label col-sm-2'])!!}
        <div class="col-sm-10">
            {!!Form::text('email',NULL,['class' => 'form-control','required' => 'required'])!!}
        </div>
    </div>

    <div class="form-group">
        {!!Form::label('active','Aktiv',['class' => 'control-label col-sm-2'])!!}
        <div class="col-sm-5 ">
            {!!Form::checkbox('active')!!}
        </div>
    </div>


    <div class="form-group">
        {!!Form::label('palaver','Palaver',['class' => 'control-label col-sm-2'])!!}
        <div class="col-sm-5 ">
            {!!Form::checkbox('palaver')!!}
        </div>
    </div>

    <div class="form-group">
        {!!Form::label('admin','Admin',['class' => 'control-label col-sm-2'])!!}
        <div class="col-sm-5 ">
            {!!Form::checkbox('admin')!!}
        </div>
    </div>

    <div class="form-group">
        {!!Form::label('flugverbot','flugverbot',['class' => 'control-label col-sm-2'])!!}
        <div class="col-sm-5 ">
            {!!Form::checkbox('flugverbot')!!}
        </div>
    </div>


    <div class="form-group">
        {!!Form::label('flugverbot_kommentar','flugverbot_kommentar',['class' => 'control-label col-sm-2'])!!}
        <div class="col-sm-5 ">
            {!!Form::text('flugverbot_kommentar',null,['class' => 'form-control col-sm-10'])!!}
        </div>
    </div>

    <div class="form-group ">
        {!!Form::label('date','Mitglied seit',['class' => 'control-label col-sm-2'])!!}
        <div class="col-sm-10">
            <div class='input-group date' id='datetimepicker1'>
                {!!Form::text('date','',['class' => 'form-control col-sm-10 '])!!}
                <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
            </div>
        </div>
        <script type="text/javascript">

            $(function () {
                $('#datetimepicker1').datetimepicker({
                    format: "YYYY-MM-DD",
                    @if(isset($user))
                    date: moment("{{$user->created_at}}"),
                    @else
                    date: moment()
                    @endif
                });
            });
        </script>
    </div>


@endif

@if(env('PALAVER_FEATURES'))
    @if(isset($user))
        <div class="form-group">
            {!!Form::label('next_palaver_entschuldigt','Entschuldigt bei nächstem Palaver',['class' => 'control-label col-sm-2'])!!}
            <div class="col-sm-5 ">
                {!!Form::checkbox('next_palaver_entschuldigt')!!}
            </div>
        </div>
    @endif
@endif


<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
    <label class="control-label col-sm-2">@if(isset($user)) Passwort ändern @else Passwort @endif </label>

    <div class="col-sm-10">
        <input type="password" class="form-control" name="password">
        @include('layouts.display_error',['field_name'=>'password'])
    </div>
</div>

<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
    <label class="control-label col-sm-2">Passwort bestätigen</label>

    <div class="col-sm-10">
        <input type="password" class="form-control" name="password_confirmation">

        @if ($errors->has('password_confirmation'))
            <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
        @endif
    </div>
</div>

