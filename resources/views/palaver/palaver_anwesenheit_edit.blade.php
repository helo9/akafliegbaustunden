@extends('layouts.layout')
@section('title','Anwesenheit')


@section('content')

    <div class="col-md-12">
        <table class="table table-hover  table-bordered">
            <thead>
            <tr>
                <td>Name</td>
                <td>Anwesend</td>
                <td>Entschuldigt</td>

            </tr>
            </thead>
    {!!  Form::open(['method'=>'post','url' => 'palaver/'.$palaver->id.'/edit','role'=>'form']) !!}
    @include('palaver.form_partial')
    {!! Form::submit('Speichern',['class' => 'btn btn-primary col-md-2 col-md-offset-5']) !!}
    {!! Form::close() !!}


@endsection