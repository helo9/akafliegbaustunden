<nav class="navbar navbar-default">
    <div class="container-fluid">
        <ul class="nav navbar-nav">
            <li><a href='/entry/create'><span>Baustunden Eintragen</span></a></li>
            <li><a href='/user/{{Auth::id()}}'><span>Meine Baustunden</span></a></li>
            <li class="dropdown"><a href="/project"><span>Übersicht</span></a></li>
            @if(env('PALAVER_FEATURES'))
                <li><a href='/palaverIndex'><span>Palaver Protokolle</span></a></li>
            @endif
            <li><a href='/fliegen'><span>Fliegen</span></a></li>


            <!-- <li><a href='/summary'><span>Zusammenfassung anzeigen</span></a></li> -->
        </ul>

        <!-- Right Side Of Navbar -->
        <ul class="nav navbar-nav navbar-right">
            @if(env('SHOW_STATISTICS') || Auth::user()->is_admin)
                <li><a href='/statistics'><span>Statistiken</span></a></li>
            @endif
        <!-- Authentication Links -->
            @if (Auth::guest())
                <li><a href="/login">Login</a></li>
            @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->full_name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        @can('create',PalaverItem::class)
                            <li><a href="/palaverItem/create"><i class="fa fa-btn fa-sign-out"></i>Aufgabe
                                    erstellen</a>
                            </li>
                        @endcan

                        @can('create',\App\models\project::class)
                            <li><a href="/project/create"><i class="fa fa-btn fa-sign-out"></i>Projekt
                                    erstellen</a>
                            </li>
                        @endcan
                        @if(env('PALAVER_FEATURES'))
                            @can('palaver')

                                <li>
                                    <a href="/palaver/create"><i class="fa fa-btn fa-sign-out"></i>Palaver</a>
                                </li>
                            @endcan
                        @endif
                        @if(Auth::user()->is_admin)
                            <li><a href="/user"><i class="fa fa-btn fa-sign-out"></i>Benutzer
                                    verwalten</a>
                            </li>
                            <li><a href="/register"><i class="fa fa-btn fa-sign-out"></i>Benutzer
                                    hinzufügen</a>
                            </li>
                        @endif
                            <li><a href="/user/{{Auth::user()->id}}/edit"><i
                                            class="fa fa-btn fa-sign-out"></i>Profil
                                bearbeiten</a>
                        </li>

                            <li><a href="/email_overview">Übersicht Email Adressen</a>
                            </li>
                            <li><a href="/datenschutz/"><i
                                            class="fa fa-btn fa-sign-out"></i>Datenschutzbestimmung</a>
                            </li>
                            <li><a href="/logout"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                    </ul>
                </li>
            @endif
        </ul>
    </div>
</nav>
