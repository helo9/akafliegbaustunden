<?php
namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/entry/create';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function username()
    {
        return 'last_name';
    }

    /*
     *
     * rejects login request for users, which are no longer members
     */
    protected function authenticated(Request $request, User $user)
    {

        // TODO this is not safe; user should not be allowed to log in in the first place. No priority, since exploitation is low impact and high effort
        if($user->status->name == 'Ausgetreten'){
            Auth::logout();
            return $this->sendFailedLoginResponse($request);
        }
    }


}
