<?php

namespace App\Http\Controllers;

use App\models\car;
use App\models\user;
use Illuminate\Http\Request;

class car_controller extends Controller
{
    public function store(Request $request) {
        $this->validate($request,
                ['seats' => 'required|numeric|min:1|max:9',
                ]);

        $car = new car;
        $car->fill($request->all());
        $this->authorize('store',$car);
        $car->save();
        return back();
    }

    public function destroy(car $car) {
        $this->authorize('destroy',$car);
        $car->delete();
        return back();
    }

    public function addPassenger(car $car,Request $request) {
        if (isset($request->IDs)) {
            $this->authorize('addMultiplePassengers', $car);
            $neueMitfahrer = collect($request->IDs)->unique();
            $neueMitfahrer=$neueMitfahrer->filter(function($value){
                return is_numeric($value);
            });

            if($neueMitfahrer->count()<= ($car->seats - $car->mitfahrer->count())){
                $car->mitfahrer()->attach($neueMitfahrer->toArray());
                return back();
            }
            else {
                \Session::flash('alert-danger','Nicht genug Plätze für alle Vorhanden');
                return back()->withInput();
            }
        }

        if (!isset($request->IDs)) {
            $this->authorize('addPassenger',$car);
            $car->mitfahrer()->attach($request->user_id);
            return back();
        }
        return back()->withInput();
    }

    public function removePassenger(car $car, User $user) {
        //policy is in carPolicy for logic reasons
        $this->authorize('remove_mitfahrer', [$car, $user]);
        $car->mitfahrer()->detach($user);
        return back();
    }
}
