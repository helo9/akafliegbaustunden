<?php

//speichert einen user der an einem bestimmten tag fliegen will

namespace App\models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\models\pilot
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $project_id
 * @property string $date
 * @property string $comment
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\models\User $user
 * @property-read \App\models\project $airplane
 * @method static \Illuminate\Database\Query\Builder|\App\models\pilot whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\pilot whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\pilot whereProjectId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\pilot whereDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\pilot whereComment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\pilot whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\pilot whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class pilot extends Model {

    protected $table = 'pilot';
    protected $fillable = ['date', 'user_id', 'project_id', 'comment'];

    public function user() {
        return $this->hasOne('App\models\user', 'id', 'user_id');
    }

    public function airplane() {
        return $this->hasOne('App\models\project', 'id', 'project_id')->withoutGlobalScopes();
    }

}
