<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;


/**
 * App\models\entry
 *
 * @property integer $id
 * @property string $date
 * @property string $description
 * @property integer $user_id
 * @property integer $palaverItem_id
 * @property float $work_time
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\models\palaverItem $palaverItem
 * @property-read \App\models\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\models\User[] $helpers
 * @property-read mixed $formatted_time
 * @property-read mixed $hours
 * @property-read mixed $minutes
 * @property-read mixed $helpers_string
 * @method static \Illuminate\Database\Query\Builder|\App\models\entry whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\entry whereDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\entry whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\entry whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\entry wherePalaverItemId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\entry whereWorkTime($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\entry whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\entry whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class entry extends Model
{

    protected $table='entries';
 //   protected $touches=['helpers']; //updates entry timestamp when helpers are changed




    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('orderBy', function ($builder) {
            $builder->orderBy('date', 'desc');
        });
    }

    public function scopeThisYear($query) {
        $dates = \Help::getStartEndDates(\Help::currentYear());
        $start_date = $dates[0];
        $end_date = $dates[1];

        return $query->where('entries.date', '>=', $start_date)->where('entries.date', '<=', $end_date);

    }


    public function scopeYear($query,$year) {

        $dates = \Help::getStartEndDates($year);
        $start_date = $dates[0];
        $end_date = $dates[1];

        return $query->where('entries.date', '>=', $start_date)->where('entries.date', '<=', $end_date);

    }

    public function palaverItem() {
        return $this->belongsTo('App\models\palaverItem', 'palaverItem_id');
    }

    public function user() {
        return $this->belongsTo('App\models\user');
    }

    public function helpers() {
        return $this->belongsToMany('App\models\user', 'entry_helpers', 'entry_id', 'user_id');
    }

    public function getFormattedTimeAttribute() {
        $abs_string=floor((int)abs($this->work_time )/ 60) . ":" . str_pad((int)abs($this->work_time) % 60, 2, "0", STR_PAD_LEFT);
        if($this->work_time > 0) {
            return $abs_string;
        }
        else return '-'.$abs_string;
    }

    public function getHoursAttribute() {
        return floor((int) $this->work_time / 60);
    }

    public function getMinutesAttribute() {
        return $this->work_time % 60;
    }

    //gibt Nutzer als String zurück (Nutzer1, Nuter2, ...
    public function getHelpersStringAttribute() {
        $users = $this->helpers;
        if ($users->isEmpty()) {
            return "";
        }
        $result = "";
        for ($x = 0; $x < sizeof($users) - 1; $x++) {
            $result = $result . $users[$x]->short_name . ', ';
        }
        return $result . $users[sizeof($users) - 1]->short_name;

    }

}

