<?php
namespace App\helpers;

use Carbon\Carbon;

class format_time{

    public static function format_time($input_minutes) {
        $minutes=$input_minutes%60;
        // floor liefert fuer negative Zahlen falsche Ergebnisse
        if ($input_minutes > 0 ) {$hours = floor($input_minutes / 60);}
        else {$hours = ceil($input_minutes / 60);}
        return $hours . " Stunden und " . abs($minutes) . " Minuten";
    }

    //Ob datum in laufendem Geschäftsjahr liegt
    public static function isCurrentYear(Carbon $date) {
        Carbon::setLocale('de');
        $start_of_current_year = Carbon::parse('first day of november ' . (self::currentYear() - 1))->local;
        $end_of_current_year = Carbon::parse('last day of october ' . self::currentYear())->local;
        return $date->gte($start_of_current_year) && $date->lt($end_of_current_year);
    }

    public static function beginOfCurrentYear()
    {
        return Carbon::create(self::currentYear()-1 ,env('BEGIN_YEAR_MONTH',11),1);
    }

    public static function currentYear() {
        if (Carbon::today()->month >= env('BEGIN_YEAR_MONTH',11)) {
            return Carbon::today()->year + 1;
        }
        return Carbon::today()->year;

    }

    public static function getStartEndDates($year) {
        $start_date = Carbon::create($year - 1, (int) env('BEGIN_YEAR_MONTH'), 01, 0, 0); //beginn des Geschäftsjahres
        if ($year == \Help::currentYear()) {
            $end_date = Carbon::today('Europe/Berlin')->setTime(23,59,59);
        } else {
            //ganzes Jahr
            $end_date = Carbon::create($year, (int) env('BEGIN_YEAR_MONTH') - 1, 31, 0, 0);
        }

        return [$start_date, $end_date];
    }

    public static function formatDate($date) {
        return Carbon::parse($date)->format('d.m.y');
    }

    public function getCurrentGeschaeftsjahr() {
        return $this->getYear(Carbon::today());
    }

    public static function getYear(Carbon $date) {
        if ($date->month >= env('BEGIN_YEAR_MONTH')) {
            return $date->year + 1;
        }
        return $date->year;
    }


}


?>
