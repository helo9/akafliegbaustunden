<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ProjectBetreuerAndPalaverItemDate extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('projects', function ($table) {
            $table->integer('user_id')->unsigned()->after('active');
            //   $table->foreign('user_id')->references('id')->on('users')->onDelete('restrict'); //TODO Möglichkeit keinen Betreuer zuweisen

        });

        Schema::table('palaverItems', function ($table) {
            $table->dateTime('date')->nullable();

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('projects', function ($table) {

            $table->dropForeign(['user_id']);
            $table->dropColumn('user_id');

        });

        Schema::table('palaverItems', function ($table) {
            $table->dropColumn('date');

        });
    }
}
