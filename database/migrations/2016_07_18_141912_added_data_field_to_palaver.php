<?php

use Illuminate\Database\Migrations\Migration;

class AddedDataFieldToPalaver extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('palaver', function ($table) {
            $table->binary('data')->after('id')->nullable();
        }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('palaver', function ($table) {
            $table->dropColumn('data');
        });
    }
}
