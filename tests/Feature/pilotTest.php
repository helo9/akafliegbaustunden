<?php

namespace Tests\Feature;

use App\models\entry;
use App\models\User;
use Faker\Generator;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Faker as Faker;

class create_user extends DuskTestCase
{
    /**
     * This test verifies a new user can be created through the web interface
     *
     * @return void
     */
    public function testCreatePilot_flugberechtigt()
    {
            $this->browse(function (Browser $browser){
                /** @var User $user_login */
                $user_login = factory(User::class)->create(['flugverbot'=>0]);
                factory(entry::class)->create(['work_time' => 4000, 'user_id' =>$user_login->id]);
                $browser->loginAs($user_login);
                $browser->visit('/fliegen');
                $browser->assertTitle('ICH WILL FLIEGEN');
                $browser->assertSeeIn('#pilot_table', $user_login->short_name);
                $browser->select("project_id");
                $browser->press('save_new_pilot');
                $browser->driver->switchTo()->alert()->accept();
                $browser->assertSee($user_login->short_name);
            });

    }
    public function testCreatePilot_banned()
    {
        $this->browse(function (Browser $browser){
            /** @var User $user_login */
            $user_login = factory(User::class)->create(['flugverbot'=>1]);
            factory(entry::class)->create(['work_time' => 4000, 'user_id' =>$user_login->id]);

            $browser->loginAs($user_login);
            $browser->visit('/fliegen');
            $browser->assertTitle('ICH WILL FLIEGEN');
            $browser->assertDontSeeIn('#pilot_table', $user_login->short_name);
            $browser->assertMissing('save_new_pilot');
            $browser->assertSee("Flugverbot");
        });
    }

    public function testCreatePilot_insufficient_time()
    {
        $this->browse(function (Browser $browser){
            /** @var User $user_login */
            $user_login = factory(User::class)->create(['flugverbot'=>0,'admin'=>0]);
            factory(entry::class)->create(['work_time' => -1, 'user_id' =>$user_login->id]); //ensure insufficient time
            $browser->loginAs($user_login);
            $browser->visit('/fliegen');
            $browser->assertTitle('ICH WILL FLIEGEN');
            $browser->screenshot('a');
            $browser->assertDontSeeIn('#pilot_table', $user_login->short_name);
            $browser->assertMissing('save_new_pilot');
            $browser->assertDontSee("Flugverbot");
        });
    }

}
